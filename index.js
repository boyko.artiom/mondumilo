const fs = require("fs");
const fsP = require("fs/promises");
const tmpP = require("tmp-promise");
const webdav = require("webdav");
const util = require("util");
const yauzl = require("yauzl");
const yazl = require("yazl");

const sources = [["world", ""], ["world_nether", "DIM-1/"], ["world_the_end", "DIM1/"]];

const davClient = webdav.createClient(
	process.argv[2],
	{authType: webdav.AuthType.Password, username: process.argv[3], password: process.argv[4]},
);

(async () => {
	await tmpP.withFile(async (outFile) => {
		const contents = await davClient.getDirectoryContents("/", {deep: true});
		console.log(contents);

		contents.sort((a, b) => -a.filename.localeCompare(b.filename));

		console.log("runTask");
		const dest = new yazl.ZipFile();
		const pipe = dest.outputStream.pipe(fs.createWriteStream(outFile.path));
		for(let i = 0; i < sources.length; i++) {
			const srcPath = contents.find(x => x.filename.startsWith("/" + sources[i][0])).filename;
			await tmpP.withFile(async (inFile) => {
				await new Promise((resolve, reject) => {
					console.log("saving", srcPath, "to", inFile.path);
					davClient.createReadStream(srcPath)
						.pipe(fs.createWriteStream(inFile.path))
						.on("finish", resolve)
						.on("error", reject);
				});
				console.log("saved file");
				const src = await util.promisify(yauzl.open)(inFile.path, {lazyEntries: true});
				src.readEntry();
				await new Promise((resolve, reject) => {
					src.on("entry", entry => {
						console.log("entry", entry.fileName);
						if(entry.fileName.startsWith(sources[i][0] + "/" + sources[i][1])) {
							src.openReadStream(entry, (err, readStream) => {
								if(err) reject(err);
								else {
									dest.addReadStream(readStream, "world/" + entry.fileName.substring(sources[i][0].length + 1));
									src.readEntry();
								}
							});
						}
						else {
							src.readEntry();
						}
					});
					src.on("end", resolve);
					src.on("error", reject);
				});
			});
		}

		console.log("finishing");

		dest.end();

		await new Promise((resolve, reject) => {
			pipe.on("close", resolve);
			pipe.on("error", reject);
		});

		fsP.copyFile(outFile.path, process.argv[5]);
	});
	console.log("done");
})()
	.then(() => {
		console.log("ended");
	})
	.catch(err => {
		console.error(err);
		process.exit(1);
	});
